package com.sampark.dsign.main;

import java.util.List;

import org.apache.log4j.Logger;

import com.sampark.dsign.dao.DBQueries;
import com.sampark.dsign.model.ContractTypeBean;
import com.sampark.dsign.model.SignatoriesBean;
import com.sampark.dsign.service.HSMService;
import com.sampark.dsign.service.SignDocumentService;

public class MasterThread {

	static Logger logger = Logger.getLogger(MasterThread.class);
	static Logger durationLogger = Logger.getLogger("DURATION");
	Long t1 = 0L;
	
	public void execute() {
		//MailService mailService = new MailService();
		DBQueries dao = new DBQueries();
		
		t1 = System.currentTimeMillis();
		//ProfileBean profileBean = new ProfileBean();
		StringBuilder message =new StringBuilder();
		message.append("");
		
		List<ContractTypeBean> contractTypeList = dao.getPendingContractTypeList();
		for(int i=0; i<contractTypeList.size();i++) {
			Integer profileSigId = contractTypeList.get(i).getProfileId();
			
			String output = dao.getOutput(profileSigId);
			contractTypeList.get(i).setDestFilePath(output);
			logger.info("Output folder location is : "+output);
			
			Integer signId = dao.getSignatoryId(profileSigId);
			
			SignatoriesBean signBean = dao.getSignatureDetail(signId);
			
			contractTypeList.get(i).setSignBean(signBean);
			
			Integer employeeId = dao.getEmployeeId(contractTypeList.get(i).getUploadedBy());
			
			contractTypeList.get(i).setEmailId(dao.getEmployeeMail(employeeId));
			logger.info("uploader employeeId --> "+employeeId +" and email id "+contractTypeList.get(i).getEmailId());
			SignatoriesBean sBean1 = HSMService.readPrivateKeyFromHSM(contractTypeList.get(i).getSignBean().getPrivateKeyLabel(),
					contractTypeList.get(i).getSignBean().getCertificateLabel()); 
			
			contractTypeList.get(i).getSignBean().setCert(sBean1.getCert());
			contractTypeList.get(i).getSignBean().setPrivateKey(sBean1.getPrivateKey());
			
			new SignDocumentService().processDocument(contractTypeList.get(i));
			
			dao.updateContractType(contractTypeList.get(i).getContractTypeId(), output);
		}
		
		logger.info("Pdf file copy stopped");
		
	}
}
