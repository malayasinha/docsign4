package com.sampark.dsign.model;

public class InvoiceRecepientBean {
	private String mail;
	private String file;
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public String getFile() {
		return file;
	}
	public void setFile(String file) {
		this.file = file;
	}
	public InvoiceRecepientBean(String mail, String file) {
		super();
		this.mail = mail;
		this.file = file;
	}
	
	
}
