package com.sampark.dsign.model;

public class B2BMailBean {
	private String emailId;
	private String subject;
	private String body;
	private String fileLocation;
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	public String getFileLocation() {
		return fileLocation;
	}
	public void setFileLocation(String fileLocation) {
		this.fileLocation = fileLocation;
	}
	@Override
	public String toString() {
		return "B2BMailBean [emailId=" + emailId + ", subject=" + subject + ", fileLocation=" + fileLocation + "]";
	}
	
	
}
