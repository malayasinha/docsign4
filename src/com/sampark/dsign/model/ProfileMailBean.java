package com.sampark.dsign.model;

public class ProfileMailBean {
	private Integer rowId;
	private Integer profileId;
	private String recepientType;
	private String emailId;
	public Integer getRowId() {
		return rowId;
	}
	public void setRowId(Integer rowId) {
		this.rowId = rowId;
	}
	public Integer getProfileId() {
		return profileId;
	}
	public void setProfileId(Integer profileId) {
		this.profileId = profileId;
	}
	public String getRecepientType() {
		return recepientType;
	}
	public void setRecepientType(String recepientType) {
		this.recepientType = recepientType;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	@Override
	public String toString() {
		return "ProfileMailBean [rowId=" + rowId + ", profileId=" + profileId + ", recepientType=" + recepientType
				+ ", emailId=" + emailId + "]";
	}
	
	
}
