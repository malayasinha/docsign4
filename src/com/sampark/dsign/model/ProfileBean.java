package com.sampark.dsign.model;

public class ProfileBean {
	private Integer rowId;
	private String profileName;
	private String profileDescription;
	private String inputFolder;
	private String outputFolder;
	
	public Integer getRowId() {
		return rowId;
	}
	public void setRowId(Integer rowId) {
		this.rowId = rowId;
	}
	public String getProfileName() {
		return profileName;
	}
	public void setProfileName(String profileName) {
		this.profileName = profileName;
	}
	public String getProfileDescription() {
		return profileDescription;
	}
	public void setProfileDescription(String profileDescription) {
		this.profileDescription = profileDescription;
	}
	public String getInputFolder() {
		return inputFolder;
	}
	public void setInputFolder(String inputFolder) {
		this.inputFolder = inputFolder;
	}
	public String getOutputFolder() {
		return outputFolder;
	}
	public void setOutputFolder(String outputFolder) {
		this.outputFolder = outputFolder;
	}
	@Override
	public String toString() {
		return "ProfileBean [rowId=" + rowId + ", profileName=" + profileName + ", profileDescription="
				+ profileDescription + ", inputFolder=" + inputFolder + ", outputFolder=" + outputFolder + "]";
	}
	

}
