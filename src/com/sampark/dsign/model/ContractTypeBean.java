package com.sampark.dsign.model;

public class ContractTypeBean {
	private Integer contractTypeId;
	private String sourceFileName;
	private String sourceFilePath;
	private Integer profileId;
	private String destFilePath;
	private SignatoriesBean signBean;
	private Integer createdBy;
	private String emailId;
	private Integer uploadedBy;
	
	public Integer getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}
	public Integer getContractTypeId() {
		return contractTypeId;
	}
	public void setContractTypeId(Integer contractTypeId) {
		this.contractTypeId = contractTypeId;
	}
	public String getSourceFileName() {
		return sourceFileName;
	}
	public void setSourceFileName(String sourceFileName) {
		this.sourceFileName = sourceFileName;
	}
	public String getSourceFilePath() {
		return sourceFilePath;
	}
	public void setSourceFilePath(String sourceFilePath) {
		this.sourceFilePath = sourceFilePath;
	}
	public Integer getProfileId() {
		return profileId;
	}
	public void setProfileId(Integer profileId) {
		this.profileId = profileId;
	}
	public String getDestFilePath() {
		return destFilePath;
	}
	public void setDestFilePath(String destFilePath) {
		this.destFilePath = destFilePath;
	}
	public SignatoriesBean getSignBean() {
		return signBean;
	}
	public void setSignBean(SignatoriesBean signBean) {
		this.signBean = signBean;
	}
	
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public Integer getUploadedBy() {
		return uploadedBy;
	}
	public void setUploadedBy(Integer uploadedBy) {
		this.uploadedBy = uploadedBy;
	}
	@Override
	public String toString() {
		return "ContractTypeBean [contractTypeId=" + contractTypeId + ", sourceFileName=" + sourceFileName
				+ ", sourceFilePath=" + sourceFilePath + ", profileId=" + profileId + ", destFilePath=" + destFilePath
				+ ", signBean=" + signBean + ", createdBy=" + createdBy + ", email_id=" + emailId + "]";
	}
	
	
}
