package com.sampark.dsign.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.sampark.dsign.model.InvoiceRecepientBean;

public class ReadFiles {
	private List<InvoiceRecepientBean> readFlatFile(File file) {
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ",";
        List<InvoiceRecepientBean> mailList = new ArrayList<InvoiceRecepientBean>();
        try {
	            br = new BufferedReader(new FileReader(file.getPath()));
	            while ((line = br.readLine()) != null) {
	
	                // use comma as separator
	            	if(line.length()<5) {
	            		continue;
	            	}
	                String[] words = line.split(cvsSplitBy);
	                //mail,file
	                mailList.add(new InvoiceRecepientBean(words[0], words[1]));
	            }
        	
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return mailList;
	}
}
