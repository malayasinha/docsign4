package com.sampark.dsign.util;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;

import net.glxn.qrgen.QRCode;
import net.glxn.qrgen.image.ImageType;

public class QRUtility {
	public static ByteArrayOutputStream writeImage(String source,String qrText) {
		PdfReader reader = null;
		PdfStamper stamper = null;
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		try {
			reader = new PdfReader(source);
						
			stamper = new PdfStamper(reader, bos);//new FileOutputStream("/media/malaya/D Drive/9063780147_qr.pdf"));
			ByteArrayOutputStream baos = QRCode.from(qrText).withSize(100, 100)
					.to(ImageType.PNG).stream();
			byte [] output = null;
			
			output = baos.toByteArray();
			Image img = Image.getInstance(output);
			
			//Image img = Image.getInstance("watermark.jpg");
			img.setAbsolutePosition(435, 700);
			BaseFont bf = BaseFont.createFont(BaseFont.HELVETICA, BaseFont.WINANSI, BaseFont.NOT_EMBEDDED);
			PdfContentByte under, over;
			int total = reader.getNumberOfPages() + 1;
			for (int i = 1; i < total; i++) {
				stamper.setRotateContents(false);
				under = stamper.getUnderContent(i);
				under.addImage(img);

			}
			stamper.close();
		} catch (IOException e) {

			e.printStackTrace();
		} catch (DocumentException e) {

			e.printStackTrace();
		}
		return bos;
	}
}
