package com.sampark.dsign.util;

import java.io.File;
import java.io.FilenameFilter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

public class SignatureUtil {
	public static Long expiringIn(Date endDate) {
		Calendar c = Calendar.getInstance();
		Date today = c.getTime();

		long diff =  - today.getTime() + endDate.getTime();

		long diffDays = diff / (24 * 60 * 60 * 1000);
		
		return diffDays;
	}
	public static boolean isNullOrBlank(String str) {
		if(str==null) {
			return true;
		} else if(str.trim().length()==0) {
			return true;
		} else {
			return false;
		}
	}
	public static String isExistAndHasPermission(String fileName) {
		File file = new File(fileName);
		String message = "";
		
		if(!file.exists()) {
			message  = fileName + " folder does not exist";
		} 
		
		if(message.length()==0) {
			if(!file.canRead()) {
				message = fileName + " folder does not have read permission";
			}
		}
		
		if(message.length()==0) {
			if(!file.canWrite()) {
				message = fileName + " folder does not have write permission";
			}
		}
		
		return message;
	}
	
	public static String isEmpty(String location) {
		String message="";
		List<File> zipFiles = SignatureUtil.getAssortedList(new File(location),".pdf");
		if(zipFiles.size()==0) {
			message = location +" folder does not contain any file for processing";
		}
		
		return message;
	}
	
	public static List<File> getAssortedList(File source,String endsWith) {
		ArrayList<File> files = new ArrayList<File>(Arrays.asList(source.listFiles(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.toLowerCase().endsWith(endsWith);
			}
		})));
		
		return files;
	}
	
	public static String getCurrentTimestamp(String pattern) {
		//String pattern = "dd-MM-yyyy HH:mm:ss";
		DateFormat df = new SimpleDateFormat(pattern);
		Date today = Calendar.getInstance().getTime();  
		String todayAsString = df.format(today);
		return todayAsString;
	}
	public static File getAssortedFile(File source,String endsWith) {
		File [] files = source.listFiles();
		String fileName = "";
		File csvFile = null;
		for(File file : files) {
			fileName = file.getName();
			if(StringUtils.equalsIgnoreCase(StringUtils.right(fileName, 3), endsWith)) {
				csvFile = file;
				break;
			}
			
		}
		
		return csvFile;
	}
	
	
}
