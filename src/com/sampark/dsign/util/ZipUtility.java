package com.sampark.dsign.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.zip.GZIPOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.commons.compress.archivers.sevenz.SevenZArchiveEntry;
import org.apache.commons.compress.archivers.sevenz.SevenZOutputFile;
import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveOutputStream;
import org.apache.commons.compress.utils.IOUtils;
import org.apache.log4j.Logger;

import com.sampark.dsign.constants.Constants;

public class ZipUtility {
	Logger logger = Logger.getLogger(ZipUtility.class);

	public void createTarFile(String sourceFolder,String type) {
		Calendar cal = Calendar.getInstance();
		Date date = cal.getTime();
		SimpleDateFormat sdf = new SimpleDateFormat("dd_MM_yyyy_hh_mm");
		
		List<File> files = Arrays.asList(new File(sourceFolder).listFiles());
		
		String backupFolder = Constants.backupPath;
		File directory = new File(backupFolder);
		if (!directory.exists()) {
			directory.mkdir();
		}
		String backupPath = backupFolder + type +"_" + sdf.format(date) + ".tar.gz";
		System.out.println("backupPath:"+backupPath);
		TarArchiveOutputStream tarOs = null;
		try {
			// Using input name to create output name
			FileOutputStream fos = new FileOutputStream(backupPath);
			GZIPOutputStream gos = new GZIPOutputStream(new BufferedOutputStream(fos));
			tarOs = new TarArchiveOutputStream(gos);
			
			for (File file : files) {
				addFilesToTarGZ(file.getAbsolutePath(), file, tarOs);
			}
			
			for (File file : files) {
				if (!file.isDirectory()) {
					file.delete();
				}
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				tarOs.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private void addFilesToTarGZ(String source, File file, TarArchiveOutputStream tos) throws IOException {
		// New TarArchiveEntry
		tos.putArchiveEntry(new TarArchiveEntry(file, source));
		if (file.isFile()) {
			FileInputStream fis = new FileInputStream(file);
			BufferedInputStream bis = new BufferedInputStream(fis);
			// Write content of the file
			IOUtils.copy(bis, tos);
			tos.closeArchiveEntry();
			fis.close();
		} else if (file.isDirectory()) {
			// no need to copy any content since it is
			// a directory, just close the outputstream
			tos.closeArchiveEntry();
			for (File cFile : file.listFiles()) {
				// recursively call the method for all the subfolders
				addFilesToTarGZ(cFile.getAbsolutePath(), cFile, tos);

			}
		}

	}

	public void compress(List<File> srcFiles) throws IOException {
		Calendar cal = Calendar.getInstance();
		Date date = cal.getTime();
		SimpleDateFormat sdf = new SimpleDateFormat("dd_MM_yyyy_hh_mm");

		String backupFolder = Constants.backupPath;
		File directory = new File(backupFolder);
		if (!directory.exists()) {
			directory.mkdir();
		}
		String name = backupFolder + "archive_" + sdf.format(date) + ".7z";

		try (SevenZOutputFile out = new SevenZOutputFile(new File(name))) {
			Iterator<File> itr = srcFiles.iterator();
			Integer length = srcFiles.size();
			while (itr.hasNext()) {

				File file = itr.next();
				addToArchiveCompression(out, file, ".");
				itr.remove();

				System.out.println(length-- + " items removed");
			}
		}
	}

	private void addToArchiveCompression(SevenZOutputFile out, File file, String dir) throws IOException {
		String name = dir + File.separator + file.getName();
		if (file.isFile()) {
			SevenZArchiveEntry entry = out.createArchiveEntry(file, name);
			out.putArchiveEntry(entry);

			FileInputStream in = new FileInputStream(file);
			byte[] b = new byte[1024];
			int count = 0;
			while ((count = in.read(b)) > 0) {
				out.write(b, 0, count);
			}
			out.closeArchiveEntry();

		} else if (file.isDirectory()) {
			File[] children = file.listFiles();
			if (children != null) {
				for (File child : children) {
					addToArchiveCompression(out, child, name);
				}
			}
		} else {
			System.out.println(file.getName() + " is not supported");
		}
	}

	public void createZipFile(List<File> srcFiles) {
		Calendar cal = Calendar.getInstance();
		Date date = cal.getTime();
		SimpleDateFormat sdf = new SimpleDateFormat("dd_MM_yyyy_hh_mm");
		String zipFileName = "archive_" + sdf.format(date) + ".zip";

		// String zipFileName = "archive.zip";
		String backupFolder = Constants.backupPath;
		File directory = new File(backupFolder);
		if (!directory.exists()) {
			directory.mkdir();
		}

		try {
			// create byte buffer
			byte[] buffer = new byte[1024];
			FileOutputStream fos = new FileOutputStream(backupFolder + zipFileName);
			ZipOutputStream zos = new ZipOutputStream(fos);
			for (int i = 0; i < srcFiles.size(); i++) {
				File srcFile = srcFiles.get(i);
				FileInputStream fis = new FileInputStream(srcFile);
				// begin writing a new ZIP entry, positions the stream to the
				// start of the entry data
				zos.putNextEntry(new ZipEntry(srcFile.getName()));
				int length;
				while ((length = fis.read(buffer)) > 0) {
					zos.write(buffer, 0, length);
				}
				zos.closeEntry();
				// close the InputStream
				fis.close();
			}
			// close the ZipOutputStream
			zos.close();

			for (File file : srcFiles) {
				if (!file.isDirectory()) {
					file.delete();
				}
			}
		} catch (IOException ioe) {
			System.out.println("Error creating zip file: " + ioe);
		}

	}

	public void createZipFileNIO(List<File> srcFiles) {
		Calendar cal = Calendar.getInstance();
		Date date = cal.getTime();
		SimpleDateFormat sdf = new SimpleDateFormat("dd_MM_yyyy_hh_mm");
		String zipFileName = "archive_" + sdf.format(date) + ".zip";

		// String zipFileName = "archive.zip";
		String backupFolder = Constants.backupPath;
		File directory = new File(backupFolder);
		if (!directory.exists()) {
			directory.mkdir();
		}

		try {
			// create byte buffer
			byte[] buffer = new byte[1024];
			FileOutputStream fos = new FileOutputStream(backupFolder + zipFileName);
			ZipOutputStream zos = new ZipOutputStream(fos);
			zos.setLevel(1);
			for (int i = 0; i < srcFiles.size(); i++) {
				File srcFile = srcFiles.get(i);
				FileInputStream fis = new FileInputStream(srcFile);

				zos.putNextEntry(new ZipEntry(srcFile.getName()));
				byte[] bytes = Files.readAllBytes(Paths.get(srcFile.getAbsolutePath()));
				zos.write(bytes, 0, bytes.length);

				zos.closeEntry();
			}

			// close the ZipOutputStream
			zos.close();

			for (File file : srcFiles) {
				if (!file.isDirectory()) {
					file.delete();
				}
			}
		} catch (IOException ioe) {
			System.out.println("Error creating zip file: " + ioe);
		}

	}

	public void deleteOldGZipFiles() {
		String backupFolder = Constants.backupPath;
		
		Integer days = -1*Integer.parseInt(Constants.fileRententionPeriod);
		
		List<File> zipFiles = SignatureUtil.getAssortedList(new File(backupFolder),".tar.gz");
		
		
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, days);
		
		Date dateToCheck = cal.getTime();
		
		cal = Calendar.getInstance();
		for(File file:zipFiles) {
			cal.set(Calendar.DATE, Integer.parseInt(file.getName().split("_")[1]));
			cal.set(Calendar.MONTH, Integer.parseInt(file.getName().split("_")[2])); cal.add(Calendar.MONTH,-1);
			cal.set(Calendar.YEAR, Integer.parseInt(file.getName().split("_")[3]));
			
			Date fileDate = cal.getTime();
			
			System.out.println(file.getName());
			System.out.println(fileDate +" is before "+ dateToCheck +": "+fileDate.before(dateToCheck));
			
			if(fileDate.before(dateToCheck)) {
				System.out.println(fileDate +" is before "+ dateToCheck);
				file.delete();
			}
			
		}
		
	}
	
	

}
