package com.sampark.dsign.exception;

import java.text.MessageFormat;
import java.util.ResourceBundle;

import com.sampark.dsign.constants.Constants;

public class AppException extends Exception {

	private static final long serialVersionUID = 1L;
	private String msgId;

	public String getMsgId() {
		return msgId;
	}

	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}

	public AppException(Throwable cause) {
		super(cause);
	}

	public AppException(String messageId) {
		super(getMessageForId(messageId, null));
		this.setMsgId(messageId);
	}

	public AppException(String messageId, Throwable throwable) {
		super(getMessageForId(messageId, null), throwable);
		this.setMsgId(messageId);
	}

	public AppException(String messageId, Object objects[]) {
		super(getMessageForId(messageId, objects));
		this.setMsgId(messageId);
	}

	public AppException(String messageId, Object objects[],
			Throwable throwable) {

		super(getMessageForId(messageId, objects), throwable);
		this.setMsgId(messageId);
	}

	private static String getMessageForId(String messageId, Object objects[]) {
		String message = messageId;
		try {
			message = ResourceBundle.getBundle(Constants.APP_EXCEPTION_BUNDLE)
					.getString(messageId);
			if (message != null) {
				{
					message = MessageFormat.format(message, objects);
				}
			} else {
				message = messageId;
			}
		} catch (Exception e) {
			message = messageId;
		}
		return message;
	}
}
