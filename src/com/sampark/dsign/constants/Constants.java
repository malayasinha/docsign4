package com.sampark.dsign.constants;

import com.sampark.dsign.resources.ResourceReader;

public class Constants {

	public static final String APP_EXCEPTION_BUNDLE = "com.sampark.dsign.resources.error";
	public static final String DB_RESOURCE_BUNDLE = "com.sampark.dsign.resources.dbresources";
	public static final String APP_RESOURCE_BUNDLE = "com.sampark.dsign.resources.application";
	
	public static final String DATASOURCE_NAME = "datasource_name";

	public static final String DB_DRIVER_NAME = "database.driver";
	public static final String DB_URL = "database.url";
	public static final String DB_USERNAME = "database.username";
	public static final String DB_PASSWORD = "database.password";
	public static final String DB_POOL_MAX_ACTIVE = "database.pool.maxActive";
	public static final String DB_POOL_MAX_IDLE = "database.pool.maxIdle";
	public static final String DB_POOL_MAX_WAIT = "database.pool.maxWait.millisec";
	public static final String DB_POOL_MIN_IDLE = "database.pool.minIdle";
	public static final String DB_POOL_VALIDATION_QUERY = "database.pool.validationQuery";
	public static final String DB_POOL_TEST_WHILE_IDLE = "database.pool.testWhileIdle";
	public static final String DB_POOL_TEST_ON_BORROW = "database.pool.testOnBorrow";
	public static final String DB_POOL_TIME_BETWEEN_EVICTIONRUNS = "database.pool.timeBetweenEvictionRuns.minutes";
	public static final String DB_POOL_MIN_EVICTABLE_IDLE_TIME = "database.pool.minEvictableIdleTime.minutes";
	public static final String DB_POOL_MIN_NUM_TSETS_PER_EVICTION_RUN = "database.pool.numTestsPerEvictionRun";

	public static String PartitionName = ResourceReader.getValueFromBundle("partition.name", Constants.APP_RESOURCE_BUNDLE);
	public static String PartitionPassword = ResourceReader.getValueFromBundle("partition.password", Constants.APP_RESOURCE_BUNDLE);
	
	public static String reason_ioe = "reason.ioexception";
	public static String reason_doe = "reason.documentexception";
	public static String reason_gse = "reason.generalsecurityexception";
	public static String reason_oe = "reason.otherexception";
	
	public static String backupPath = ResourceReader.getValueFromBundle("backup.file.path", Constants.APP_RESOURCE_BUNDLE);
	public static String projPath = ResourceReader.getValueFromBundle("proj.file.path", Constants.APP_RESOURCE_BUNDLE);
	
	public static String fileRententionPeriod = ResourceReader.getValueFromBundle("file.retention.period", Constants.APP_RESOURCE_BUNDLE);
	
	public static final String MAILUSERID = ResourceReader.getValueFromBundle("mail.userid", Constants.APP_RESOURCE_BUNDLE);
	public static final String MAILHOST=ResourceReader.getValueFromBundle("mail.ip", Constants.APP_RESOURCE_BUNDLE);
	public static final String MAILPORT=ResourceReader.getValueFromBundle("mail.port", Constants.APP_RESOURCE_BUNDLE);
	public static final String MAILEMAIL=ResourceReader.getValueFromBundle("mail.emailid", Constants.APP_RESOURCE_BUNDLE);
	public static final String MAILAUTH=ResourceReader.getValueFromBundle("mail.auth", Constants.APP_RESOURCE_BUNDLE);
	public static final String MAILPASS = ResourceReader.getValueFromBundle("mail.password", Constants.APP_RESOURCE_BUNDLE);
	public static final String ENVIRONMENT = ResourceReader.getValueFromBundle("app.env", Constants.APP_RESOURCE_BUNDLE);
	public static final String SIGNPROFILE = ResourceReader.getValueFromBundle("sign.profile", Constants.APP_RESOURCE_BUNDLE);
	
	public static String MAILENABLE = ResourceReader.getValueFromBundle("mail.enable", Constants.APP_RESOURCE_BUNDLE);
	
	public static Integer Failed = 0;
	
}
