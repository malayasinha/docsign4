package com.sampark.dsign.thread;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.GeneralSecurityException;
import java.security.cert.X509Certificate;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfAnnotation;
import com.itextpdf.text.pdf.PdfFormField;
import com.itextpdf.text.pdf.PdfFormXObject;
import com.itextpdf.text.pdf.PdfName;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfSignatureAppearance;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfString;
import com.itextpdf.text.pdf.PdfTemplate;
import com.itextpdf.text.pdf.security.BouncyCastleDigest;
import com.itextpdf.text.pdf.security.CertificateInfo;
import com.itextpdf.text.pdf.security.ExternalDigest;
import com.itextpdf.text.pdf.security.ExternalSignature;
import com.itextpdf.text.pdf.security.MakeSignature;
import com.itextpdf.text.pdf.security.MakeSignature.CryptoStandard;
import com.itextpdf.text.pdf.security.PrivateKeySignature;
import com.sampark.dsign.constants.Constants;
import com.sampark.dsign.dao.DBQueries;
import com.sampark.dsign.model.ProfileSignatories;
import com.sampark.dsign.model.SignatoriesBean;
import com.sampark.dsign.resources.ResourceReader;
import com.sampark.dsign.util.QRUtility;
import com.sampark.dsign.util.RandomText;

import sun.font.FontFamily;

public class SignDocumentThread {
	static Logger logger = Logger.getLogger(SignDocumentThread.class);
	static Logger durationLogger = Logger.getLogger("DURATION");
	Long t1 = 0L;
	private String source;
	private String destination;

	private SignatoriesBean profileSign;

	private static Font smallNormal = new Font(Font.FontFamily.TIMES_ROMAN, 6);
	private static Font smallBold = new Font(Font.FontFamily.TIMES_ROMAN, 7, Font.BOLD);

	public SignDocumentThread(String source, String destination, SignatoriesBean profileSign) {
		super();
		this.source = source;
		this.destination = destination;
		this.profileSign = profileSign;
	}

	public void run() {
		DBQueries dao = new DBQueries();
		try {
			signPdfFirstTime(source, destination, profileSign);
			//signPdfFirstDemo(source, destination, profileSign);
			
		} catch (IOException ioe) {
			ioe.printStackTrace();
		} catch (DocumentException de) {
			de.printStackTrace();
		} catch (GeneralSecurityException gse) {
			gse.printStackTrace();
		} catch (Exception e) {
			Constants.Failed++;
		}
	}

	public void signPdfFirstTime(String src, String dest, SignatoriesBean bean)
			throws FileNotFoundException, IOException, DocumentException, GeneralSecurityException {
		// QR Code attachment
		/*ByteArrayOutputStream baos = QRUtility.writeImage( src, "Tatasky Limited");
		ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
		PdfReader reader = new PdfReader(bais);*/
		
		PdfReader reader = new PdfReader(src);
		ByteArrayOutputStream out = new ByteArrayOutputStream(); 
		PdfStamper stp1 = new PdfStamper(reader, out, '\3', true);
		 
		PdfFormField sig = PdfFormField.createSignature(stp1.getWriter());
		sig.setWidget(new Rectangle(bean.getX(), bean.getY(), bean.getX() + bean.getWidth(), bean.getY() + bean.getHeight()), null);
		sig.setFlags(PdfAnnotation.FLAGS_PRINT);
		sig.put(PdfName.DA, new PdfString("/Helv 0 Tf 0 g"));
		sig.setFieldName("Signature4");
		sig.setPage(1);
		int pageNo = stp1.getReader().getNumberOfPages();
		// Attached the blank signature field to the existing document
		//stp1.addAnnotation(sig, 1);
		for(int p=1; p<=pageNo; p++)
			stp1.addAnnotation(sig, p);
		
		stp1.close();
		out.close(); 
		 
		FileOutputStream fos = new FileOutputStream(dest);
		reader = new PdfReader(out.toByteArray()); 
		PdfStamper stamper = PdfStamper.createSignature(reader, fos, '\0');

		// appearance
		pageNo = stamper.getReader().getNumberOfPages();
		PdfSignatureAppearance appearance = stamper.getSignatureAppearance();
		
		appearance.setAcro6Layers(false);
		appearance.setLayer4Text(PdfSignatureAppearance.questionMark);
		// appearance.setCertificationLevel(PdfSignatureAppearance.CERTIFIED_NO_CHANGES_ALLOWED);
		appearance.setVisibleSignature("Signature4");
				/*new Rectangle(bean.getxAxis(), bean.getyAxis(), bean.getxAxis() + bean.getSignSize()+40, bean.getyAxis() + bean.getSignSize()), pageNo,
				bean.getSignBean().getName());*/

		// creating template for layer 0
		PdfTemplate n0 = appearance.getLayer(0);
		float x = n0.getBoundingBox().getLeft();
		float y = n0.getBoundingBox().getBottom();
		float w = n0.getBoundingBox().getWidth();
		float h = n0.getBoundingBox().getHeight();

		n0.setColorFill(BaseColor.LIGHT_GRAY);
		n0.rectangle(x, y, w, h);
		n0.fill();

		// creating template for layer 2
		PdfTemplate n2 = appearance.getLayer(2);
		ColumnText ct = new ColumnText(n2);
		ct.setSimpleColumn(n2.getBoundingBox());

		Paragraph paragraph = new Paragraph();
		addEmptyLine(paragraph, 1);
		ct.addElement(paragraph);

		paragraph = new Paragraph("Certification signature by " + bean.getName(), smallNormal);
		paragraph.setAlignment(Element.ALIGN_LEFT);
		paragraph.setIndentationLeft(0);
		ct.addElement(paragraph);

		paragraph = new Paragraph("Digitally Signed by " + bean.getName(), smallBold);
		paragraph.setAlignment(Element.ALIGN_LEFT);
		paragraph.setIndentationLeft(0);
		ct.addElement(paragraph);

		paragraph = new Paragraph("Date " + new Date(), smallBold);
		paragraph.setAlignment(Element.ALIGN_LEFT);
		paragraph.setIndentationLeft(0);
		ct.addElement(paragraph);

		ct.go();

		// digital signature
		ExternalSignature es = new PrivateKeySignature(bean.getPrivateKey(), "SHA-256", "LunaProvider");
		ExternalDigest digest = new BouncyCastleDigest();
		MakeSignature.signDetached(appearance, digest, es, bean.getCert(), null, null, null, 0,
				CryptoStandard.CMS);
		stamper.close();
		fos.close();
	}

	public void signPdfFirstDemo(String src, String dest, SignatoriesBean bean)
			throws IOException, DocumentException, GeneralSecurityException {
		System.out.println("Signing " + src + " with " + bean.getName());
		copyFileUsingStream(new File(src), new File(dest));
	}

	private void moveFile(String source) {
		Path temp = null;
		File file = new File(source);
		String dest = "/Digisigntest/input1/backup/" + file.getName();

		try {
			temp = Files.move(Paths.get(source), Paths.get(dest));
		} catch (IOException e) {
			e.printStackTrace();
		}

		if (temp != null) {
			System.out.println("File renamed and moved successfully");
		} else {
			System.out.println("Failed to move the file");
		}
	}

	private static void addEmptyLine(Paragraph paragraph, int number) {
		for (int i = 0; i < number; i++) {
			paragraph.add(new Paragraph(" "));
		}
	}

	private static void copyFileUsingStream(File source, File dest) throws IOException {
		InputStream is = null;
		OutputStream os = null;
		try {
			is = new FileInputStream(source);
			os = new FileOutputStream(dest);
			byte[] buffer = new byte[1024];
			int length;
			while ((length = is.read(buffer)) > 0) {
				os.write(buffer, 0, length);
			}
		} finally {
			is.close();
			os.close();
		}
	}
}


/*
public void signPdfFirstTime(String src, String dest, ProfileSignatories bean)
throws FileNotFoundException, IOException, DocumentException, GeneralSecurityException {
// QR Code attachment
//ByteArrayOutputStream baos = QRUtility.writeImage( src, "Tatasky Limited");
//ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
//PdfReader reader = new PdfReader(bais);

PdfReader reader = new PdfReader(src);
FileOutputStream fos = new FileOutputStream(dest);
PdfStamper stamper = PdfStamper.createSignature(reader, fos, '\0');

// appearance
int pageNo = stamper.getReader().getNumberOfPages();
PdfSignatureAppearance appearance = stamper.getSignatureAppearance();

appearance.setAcro6Layers(false);
appearance.setLayer4Text(PdfSignatureAppearance.questionMark);
// appearance.setCertificationLevel(PdfSignatureAppearance.CERTIFIED_NO_CHANGES_ALLOWED);
appearance.setVisibleSignature(
	new Rectangle(bean.getxAxis(), bean.getyAxis(), bean.getxAxis() + bean.getSignSize()+40, bean.getyAxis() + bean.getSignSize()), pageNo,
	bean.getSignBean().getName());

// creating template for layer 0
PdfTemplate n0 = appearance.getLayer(0);
float x = n0.getBoundingBox().getLeft();
float y = n0.getBoundingBox().getBottom();
float w = n0.getBoundingBox().getWidth();
float h = n0.getBoundingBox().getHeight();

n0.setColorFill(BaseColor.LIGHT_GRAY);
n0.rectangle(x, y, w, h);
n0.fill();

// creating template for layer 2
PdfTemplate n2 = appearance.getLayer(2);
ColumnText ct = new ColumnText(n2);
ct.setSimpleColumn(n2.getBoundingBox());

Paragraph paragraph = new Paragraph();
addEmptyLine(paragraph, 1);
ct.addElement(paragraph);

paragraph = new Paragraph("Certification signature by " + bean.getSignBean().getName(), smallNormal);
paragraph.setAlignment(Element.ALIGN_LEFT);
paragraph.setIndentationLeft(0);
ct.addElement(paragraph);

paragraph = new Paragraph("Digitally Signed by " + bean.getSignBean().getName(), smallBold);
paragraph.setAlignment(Element.ALIGN_LEFT);
paragraph.setIndentationLeft(0);
ct.addElement(paragraph);

paragraph = new Paragraph("Date " + new Date(), smallBold);
paragraph.setAlignment(Element.ALIGN_LEFT);
paragraph.setIndentationLeft(0);
ct.addElement(paragraph);

ct.go();

// digital signature
ExternalSignature es = new PrivateKeySignature(bean.getSignBean().getPrivateKey(), "SHA-256", "LunaProvider");
ExternalDigest digest = new BouncyCastleDigest();
MakeSignature.signDetached(appearance, digest, es, bean.getSignBean().getCert(), null, null, null, 0,
	CryptoStandard.CMS);

}*/