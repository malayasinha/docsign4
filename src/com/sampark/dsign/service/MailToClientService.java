package com.sampark.dsign.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.sampark.dsign.constants.Constants;
import com.sampark.dsign.dao.DBQueries;
import com.sampark.dsign.model.B2BMailBean;
import com.sampark.dsign.model.EmailContentBean;
import com.sampark.dsign.model.InvoiceRecepientBean;
import com.sampark.dsign.model.ProfileMailBean;
import com.sampark.dsign.thread.SignDocumentThread;
import com.sampark.dsign.util.SignatureUtil;
import com.sampark.dsign.util.ZipUtility;
import com.sun.corba.se.impl.orbutil.closure.Constant;

public class MailToClientService {
	static Logger logger = Logger.getLogger(MailToClientService.class);
	
	public Integer sendMail(String source,String destination) {
		//Get csv file from source
		Integer metaDataFilesCount = 0;
		List<File> files = SignatureUtil.getAssortedList(new File(source),"txt");
		List<InvoiceRecepientBean> recipientList = new ArrayList<>();
		metaDataFilesCount = files.size(); 
		if(metaDataFilesCount>0) {
			for(File file:files) {
				recipientList = new ArrayList<>();
				recipientList = readFlatFile(file);
				System.out.println("Item : " + file.getName() + " Count : " + recipientList.size());
				for(InvoiceRecepientBean bean : recipientList) {
		    		prepareMail("Type","Please find attached the signed invoice.",destination+bean.getFile(),bean.getMail());
				}
			}
		}
			
		return metaDataFilesCount;
	}
	
	private List<InvoiceRecepientBean> readFlatFile(File file) {
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ",";
        List<InvoiceRecepientBean> mailList = new ArrayList<InvoiceRecepientBean>();
        try {
	            br = new BufferedReader(new FileReader(file.getPath()));
	            while ((line = br.readLine()) != null) {
	
	                // use comma as separator
	            	if(line.length()<5) {
	            		continue;
	            	}
	                String[] words = line.split(cvsSplitBy);
	                //mail,file
	                mailList.add(new InvoiceRecepientBean(words[0], words[1]));
	            }
        	
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return mailList;
	}
	
	private void prepareMail(String type, String message,String fileLocation,String mailId) {
		String email = getTemplateContent("body");
		//email = StringUtils.replace(email, "{MESSAGE}", message.toString());
		
		B2BMailBean mailContent = new B2BMailBean();
		mailContent.setBody(email.toString());
		mailContent.setEmailId(mailId);
		mailContent.setSubject("Tata Sky Sale Invoice");
		mailContent.setFileLocation(fileLocation);
		
		sendMail(mailContent);
		
	}
	
	
	private void sendMail(B2BMailBean content) {
		String password = Constants.MAILPASS.equals("null")?null:Constants.MAILPASS;

		Properties props = new Properties();
		props.put("mail.smtp.host", Constants.MAILHOST); // SMTP Host
		props.put("mail.smtp.port", Constants.MAILPORT); // TLS Port
		props.put("mail.smtp.auth", Constants.MAILAUTH); // enable authentication
		//props.put("mail.smtp.starttls.enable", "true"); // enable STARTTLS

		// create Authenticator object to pass in Session.getInstance argument
		Authenticator authenticator = new Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(Constants.MAILUSERID,password);//password
			}
		};
		Session session = Session.getInstance(props, authenticator);
		logger.info(content);
		if(Constants.MAILENABLE.equals("Y")) {
			sendEmailTo(session, Constants.MAILEMAIL, content);
		} else {
			logger.info("Mail Functionality is disabled");
		}
		
	}

	private void sendEmailTo(Session session, String fromEmail,B2BMailBean content) {
		DBQueries db = new DBQueries();
		String fileName = "";
		try {
			MimeMessage msg = new MimeMessage(session);
			// set message headers
			msg.addHeader("Content-type", "text/HTML; charset=UTF-8");
			msg.addHeader("format", "flowed");
			msg.addHeader("Content-Transfer-Encoding", "8bit");
			
			msg.setFrom(new InternetAddress(fromEmail, "Digisign"));
			
			msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(content.getEmailId(), false));
			
			/*if(content.getEmailList() != null) {
				for(ProfileMailBean bean:content.getEmailList()) {
					if(bean.getRecepientType().equals("TO")) {
						
					} else {
						msg.addRecipients(Message.RecipientType.CC, InternetAddress.parse(bean.getEmailId(), false));
					}
				}
			}*/
						
			msg.setSubject(content.getSubject(), "UTF-8");
			//msg.setContent(content.getBody(), "text/html; charset=utf-8");
			
			///// *8888888888888888888888888888888888888888888888888888888888
			// Create the message part
			Multipart multipart = new MimeMultipart();

			BodyPart htmlBodyPart = new MimeBodyPart();
			htmlBodyPart.setContent(content.getBody(),"text/html; charset=utf-8");
			multipart.addBodyPart(htmlBodyPart);

			// Part two is attachment
			BodyPart attachmentBodyPart = new MimeBodyPart();
			fileName = content.getFileLocation();
			DataSource source = new FileDataSource(fileName);
			attachmentBodyPart.setDataHandler(new DataHandler(source));
			attachmentBodyPart.setFileName(new File(fileName).getName());
			System.out.println("filename:"+fileName);
			System.out.println("name:"+new File(fileName).getName());
			multipart.addBodyPart(attachmentBodyPart);

			
			// Send the complete message parts
			msg.setContent(multipart);

			////// 888888888888888888888888888888888888888888888888888888888888
			msg.setSentDate(new Date());
			msg.saveChanges();
			Transport.send(msg);
			db.insertMailHistory(1,new File(fileName).getName(),content.getEmailId(),"SUCCESS");
			System.out.println("EMail Sent Successfully!!");
		} catch (Exception e) {
			db.insertMailHistory(1,new File(fileName).getName(),content.getEmailId(),"FAILED");
			System.out.println(e.getCause());
			e.printStackTrace();
			
		}
	}
	private String getTemplateContent(String type) {
		String fileName = type+".txt";
	
		File file = new File(Constants.projPath + fileName);
		FileReader fr=null;
		String line="";
		StringBuilder emailBody = new StringBuilder();
		logger.info("Mail body picked from "+file.getAbsolutePath());
		try {
			fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);
			while((line = br.readLine()) != null){
			    emailBody.append(line);
			}
			br.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return emailBody.toString();
	}
}
