/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sampark.dsign.service;

import java.io.FileOutputStream;
import java.math.BigInteger;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.Security;
import java.security.cert.Certificate;
import java.security.interfaces.RSAPublicKey;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;

import org.apache.log4j.Logger;

import com.itextpdf.text.Document;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfSignatureAppearance;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.security.BouncyCastleDigest;
import com.itextpdf.text.pdf.security.ExternalDigest;
import com.itextpdf.text.pdf.security.ExternalSignature;
import com.itextpdf.text.pdf.security.MakeSignature;
import com.itextpdf.text.pdf.security.MakeSignature.CryptoStandard;
import com.itextpdf.text.pdf.security.PrivateKeySignature;
import com.safenetinc.luna.LunaAPI;
import com.safenetinc.luna.LunaSlotManager;
import com.safenetinc.luna.LunaTokenObject;
import com.safenetinc.luna.provider.key.LunaKey;

public class DocumentService {
	static Logger logger = Logger.getLogger(DocumentService.class);

	public static LunaSlotManager hsmConnection = LunaSlotManager.getInstance();
	public static String PartitionName = "Tatasky@1"; // "esignaturePartitionName";
	public static String PartitionPassword = "hsm@part1"; //"esignaturePartitionPassword";
	public static String CertLabel = "private_key";//"X-le-26cbddac-a565-46fe-839f-aee9e7a1b990";  //"MohitTestingKey-private"; // =
																// "esignatureCertLabel";

	public static void main(String args[]) {
		
		String documentPath = "/Digisigntest/input1/File11.pdf";
		String signedDocumentPath = "/Digisigntest/output1/File11_signed.pdf";
		//String signedDocumentPath2 = "/home/ec2-user/output/signed_testpdf_2.pdf";

		DocumentService doc = new DocumentService();
		try {
			doc.signPdf(documentPath, signedDocumentPath, true, false);
			//doc.signPdf(documentPath, signedDocumentPath2, true, true);

		} catch (Exception e) {
			logger.info(e.getMessage());
			e.printStackTrace();
		}

	}

	public void signPdf(String src, String dest, boolean certified, boolean graphic) throws Exception {
		// private key and certificate
		//resolveLunaSlotManagerInstance();
		System.out.println("Using the certificate Label:"+CertLabel);
		if (Security.getProvider("LunaProvider") == null) {
			Security.insertProviderAt(new com.safenetinc.luna.provider.LunaProvider(), 2);
		}
		System.out.println("hsm connections starts:"+new Timestamp(System.currentTimeMillis()));
		hsmConnection.login(PartitionName, PartitionPassword);

		System.out.println("hsm connections ends:"+new Timestamp(System.currentTimeMillis()));
		KeyStore ks = KeyStore.getInstance("Luna");
		
		System.out.println(ks+"kstore store:"+new Timestamp(System.currentTimeMillis()));
		ks.load(null, PartitionPassword.toCharArray());
		System.out.println("kstore load:"+new Timestamp(System.currentTimeMillis()));
		PrivateKey privateKey = (PrivateKey) ks.getKey(CertLabel, null);
		System.out.println(privateKey+"Get Key:"+new Timestamp(System.currentTimeMillis()));
		Certificate certificate = ks.getCertificate("PAVAN KUMAR MUKTAPURAM");
		System.out.println(certificate+"CERTIFICATE:"+new Timestamp(System.currentTimeMillis()));
		
		Certificate[] chain = new Certificate[1];
		chain[0] = certificate;
		System.out.println(chain[0]+"post cert change:"+new Timestamp(System.currentTimeMillis()));
		Enumeration<String> aliases = ks.aliases();
		System.out.println(aliases+"after alias :"+new Timestamp(System.currentTimeMillis()));
		while (aliases.hasMoreElements()) {
			String names = aliases.nextElement();
			System.out.println(names);
		}
		System.out.println("after while:"+new Timestamp(System.currentTimeMillis()));
		System.out.println("\n" + "certificate" + chain);

		// reader and stamper
		PdfReader reader = new PdfReader(src);

		PdfStamper stamper = PdfStamper.createSignature(reader, new FileOutputStream(dest), '\0');
		System.out.println("SIgnature create:"+new Timestamp(System.currentTimeMillis()));
		
		// appearance
		PdfSignatureAppearance appearance = stamper.getSignatureAppearance();

		Calendar cal = Calendar.getInstance();
		Date date = new Date();
		cal.setTime(date);

		int m = stamper.getReader().getNumberOfPages();

		appearance.setSignDate(cal);
/*		appearance.setReason("It's test.");
		appearance.setLocation("Gurgaon");*/
		appearance.setAcro6Layers(false);
		appearance.setLayer4Text(PdfSignatureAppearance.questionMark);
		appearance.setVisibleSignature(new Rectangle(300, 200, 400, 300), m, "Signature");
		
		if (certified)
			appearance.setCertificationLevel(PdfSignatureAppearance.CERTIFIED_NO_CHANGES_ALLOWED);
		if (graphic) {
			appearance.setSignatureGraphic(Image.getInstance("/home/ec2-user/images.png"));
			appearance.setRenderingMode(PdfSignatureAppearance.RenderingMode.GRAPHIC);
		}
		
        System.out.println("External Signature starts:"+new Timestamp(System.currentTimeMillis()));
		ExternalSignature es = new PrivateKeySignature(privateKey, "SHA-256", "LunaProvider");
		ExternalDigest digest = new BouncyCastleDigest();
		System.out.println("Make signature starts:"+new Timestamp(System.currentTimeMillis()));
		MakeSignature.signDetached(appearance, digest, es, chain, null, null, null, 0, CryptoStandard.CMS);
		
		System.out.println("Make signature ends:"+new Timestamp(System.currentTimeMillis()));
		System.out.println("..........Document Signed...........");

	}

	private static PrivateKey getPrivateKeyLabel() throws Exception {

		if (Security.getProvider("LunaProvider") == null) {
			Security.insertProviderAt(new com.safenetinc.luna.provider.LunaProvider(), 2);
		}

		if (!hsmConnection.isLoggedIn()) {
			hsmConnectionLogin();
		}
		KeyStore ks = KeyStore.getInstance("Luna");
		ks.load(null, null);
		RSAPublicKey pub = (RSAPublicKey) ks.getCertificate(CertLabel).getPublicKey();
		BigInteger modulus = pub.getModulus();
		int flag = 1;
		String keyStoreObj = "";
		LunaTokenObject privkey = null;
		Enumeration<String> aliases = ks.aliases();
		while (aliases.hasMoreElements() && flag != 0) {
			keyStoreObj = aliases.nextElement();
			if (ks.isKeyEntry(keyStoreObj)) {
				privkey = LunaTokenObject.LocateKeyByAlias(keyStoreObj);
				if (privkey.GetSmallAttribute(LunaAPI.CKA_CLASS) == 3) {
					byte[] mudulus = privkey.GetLargeAttribute(LunaAPI.CKA_MODULUS);
					BigInteger compare = LunaKey.AttributeToBigInteger(mudulus);
					flag = modulus.compareTo(compare);
				}
			}
		}

		return (PrivateKey) LunaKey.LocateKeyByAlias(keyStoreObj);

	}

	private static void refreshHsmConnection() throws Exception {
		try {
			resolveLunaSlotManagerInstance();
			hsmConnectionLogin();
		} catch (Throwable t) {
			throw new Exception(
					"Unable to login to the Hardware Storage Module (HSM). E-signing can't be completed without access to a certificate",
					t);
		}
	}

	private static void hsmConnectionLogin() {
		if (!hsmConnection.isLoggedIn()) {
			hsmConnection.login(PartitionName, PartitionPassword);
		}

	}

	private static void resolveLunaSlotManagerInstance() throws Exception {
		if (hsmConnection == null) {
			if (hsmConnection.isTokenPresent(hsmConnection.findSlotFromLabel(PartitionName))) {
				hsmConnection = LunaSlotManager.getInstance();
				hsmConnection.reinitialize();

			}
		} else {
			throw new Exception("LunaSlotManager did not return an instance.");
		}
	}

	private static Certificate[] getcertificatechain() throws Exception {
		try {

			if (Security.getProvider("LunaProvider") == null) {
				Security.insertProviderAt(new com.safenetinc.luna.provider.LunaProvider(), 2);
			}
			if (!hsmConnection.isLoggedIn()) {
				refreshHsmConnection();
			}
			KeyStore ks = KeyStore.getInstance("Luna");
			ks.load(null, null);

			Certificate[] chain = new Certificate[1];
			chain[0] = ks.getCertificate(CertLabel);
			// hsmConnection.logout();
			return chain;
		} catch (Throwable t) {			
			t.printStackTrace();
			throw new Exception(
					"Unable to retrieve resources from the Hardware Storage Module (HSM). E-signing can't be completed without a private key and certificate chain",
					t);
		}
	}

	private static void createPdf(String filename) throws Exception {
		// step 1
		Document document = new Document();
		// step 2
		PdfWriter.getInstance(document, new FileOutputStream(filename));
		// step 3
		document.open();
		// step 4
		document.add(new Paragraph("Hello World!"));
		// step 5
		document.close();
	}
}
