package com.sampark.dsign.service;

import java.io.File;
import java.io.FilenameFilter;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.log4j.Logger;

import com.sampark.dsign.model.ContractTypeBean;
import com.sampark.dsign.thread.SignDocumentThread;

public class SignDocumentService {
	static Logger logger = Logger.getLogger(DocumentService.class);
	static Logger durationLogger = Logger.getLogger("DURATION");
	Long t1 = 0L;
	
	public void processDocument(ContractTypeBean bean) {
		logger.info("StartTime:"+new Timestamp(System.currentTimeMillis()));
		MailService mailService = new MailService();
		
		File sourcePath = new File(bean.getSourceFilePath());
		String destinationPath = bean.getDestFilePath();
		File file = new File(bean.getSourceFileName());
		try {
			logger.info("Processing the pdf file "+file.getName());
			SignDocumentThread worker = new SignDocumentThread(sourcePath+File.separator+file.getName(), 
					destinationPath+File.separator+file.getName(),bean.getSignBean());
            worker.run();
            
            
		} catch (Exception e) {
			logger.info(e.getMessage());
			e.printStackTrace();
		}
	
		//mailService.prepareMail("info", profile.getRowId(), arg);
	
		
	}
	
	private List<File> getList(File source) {
		//ArrayList<File> files = new ArrayList<File>(Arrays.asList(source.listFiles()));
		ArrayList<File> files = new ArrayList<File>(Arrays.asList(source.listFiles(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.toLowerCase().endsWith(".pdf");
			}
		})));	
		
		Collections.sort(files, new Comparator<File>() {
		    @Override
		    public int compare(File o1, File o2) {
		        Long l1 = (Long)o1.lastModified();
		        Long l2 = (Long)o2.lastModified();
		    	return l1.compareTo(l2);
		    }
		});
		if(files.size()>=10000) {
			return files.subList(0, 50);
		} else {
			return files.subList(0, files.size());
		}
	}

}
