package com.sampark.dsign.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import com.safenetinc.luna.exception.LunaException;
import com.sampark.dsign.db.DBConnectionUtil;
import com.sampark.dsign.model.ContractTypeBean;
import com.sampark.dsign.model.ProfileBean;
import com.sampark.dsign.model.ProfileMailBean;
import com.sampark.dsign.model.ProfileSignatories;
import com.sampark.dsign.model.SignatoriesBean;
import com.sampark.dsign.service.HSMService;
import com.sampark.dsign.util.SignatureUtil;

public class DBQueries {
	static Logger logger = Logger.getLogger(DBQueries.class);
	
	public Boolean isExist() {
		Connection dbConn = null;
		Boolean isExist = false;

		ResultSet rs = null;
		PreparedStatement pstmt = null;
		String query = "Select * from profile";

		try {
			dbConn = DBConnectionUtil.getDBConnection();
			pstmt = dbConn.prepareStatement(query);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				isExist = true;
			}
		} catch (Exception exe) {
			logger.error(exe.toString());
			exe.printStackTrace();
		} finally {
			DBConnectionUtil.releaseResources(pstmt, rs);
			DBConnectionUtil.releaseResources(dbConn);
		}
		return isExist;
	}

	public List<ProfileBean> getProfiles() {
		ProfileBean bean = new ProfileBean();
		List<ProfileBean> list = new ArrayList<ProfileBean>();
		/*ProfileBean bean = new ProfileBean();
		bean.setInputFolder("/home/ec2-user/input");
		bean.setOutputFolder("/home/ec2-user/output");
		bean.setProfileDescription("Description");
		bean.setProfileName("Profile 1"); bean.setRow_id(1);*/
		  
		
		Connection dbConn = null;

		ResultSet rs = null;
		PreparedStatement pstmt = null;
		String query = "Select * from profile where status = 'ACTIVE'";

		try {
			dbConn = DBConnectionUtil.getDBConnection();
			pstmt = dbConn.prepareStatement(query);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				bean = new ProfileBean();
				bean.setRowId(rs.getInt("row_id"));
				bean.setProfileName(rs.getString("profile_name"));
				bean.setProfileDescription(rs.getString("profile_description"));
				bean.setInputFolder(rs.getString("input_folder"));
				bean.setOutputFolder(rs.getString("output_folder"));
				
				list.add(bean);
			}
		} catch (Exception exe) {
			logger.error(exe.toString());
			exe.printStackTrace();
		} finally {
			DBConnectionUtil.releaseResources(pstmt, rs);
			DBConnectionUtil.releaseResources(dbConn);
		}

		return list;
	}
	
	public ProfileBean getProfileDetail(String profileName) {
		ProfileBean bean = new ProfileBean();
		
		Connection dbConn = null;

		ResultSet rs = null;
		PreparedStatement pstmt = null;
		String query = "Select * from profile where status = 'ACTIVE' and profile_name=?";

		try {
			dbConn = DBConnectionUtil.getDBConnection();
			pstmt = dbConn.prepareStatement(query);
			pstmt.setString(1, profileName);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				bean = new ProfileBean();
				bean.setRowId(rs.getInt("row_id"));
				bean.setProfileName(rs.getString("profile_name"));
				bean.setProfileDescription(rs.getString("profile_description"));
				bean.setInputFolder(rs.getString("input_folder"));
				bean.setOutputFolder(rs.getString("output_folder"));
			}
		} catch (Exception exe) {
			logger.error(exe.toString());
			exe.printStackTrace();
		} finally {
			DBConnectionUtil.releaseResources(pstmt, rs);
			DBConnectionUtil.releaseResources(dbConn);
		}

		return bean;
	}

	public ProfileSignatories getProfileSign(Integer id) {
		ProfileSignatories bean = new ProfileSignatories();
		SignatoriesBean signatories = new SignatoriesBean();
		
		Connection dbConn = null;

		ResultSet rs = null;
		PreparedStatement pstmt = null;
		String query = "Select * from profile_signatories where profile_id = ?";

		try {
			dbConn = DBConnectionUtil.getDBConnection();
			pstmt = dbConn.prepareStatement(query);
			pstmt.setInt(1, id);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				bean = new ProfileSignatories();
				bean.setRowId(rs.getInt("row_id"));
				bean.setProfileId(rs.getInt("profile_id"));
				bean.setSignId(rs.getInt("signatory_id"));
				bean.setxAxis(rs.getInt("x_axis"));
				bean.setyAxis(rs.getInt("y_axis"));
				bean.setSignSize(rs.getInt("sign_size"));
				bean.setPages(rs.getString("pages"));
				
				
			}
		} catch (Exception exe) {
			logger.error(exe.toString());
			exe.printStackTrace();
		} finally {
			DBConnectionUtil.releaseResources(pstmt, rs);
			DBConnectionUtil.releaseResources(dbConn);
		}

		return bean;
	}
	
	public SignatoriesBean getSignatories(Integer id) { 
		SignatoriesBean bean = new SignatoriesBean();
		Connection dbConn = null;

		ResultSet rs = null;
		PreparedStatement pstmt = null;
		String query = "Select * from signatories where row_id = ?";

		try {
			dbConn = DBConnectionUtil.getDBConnection();
			pstmt = dbConn.prepareStatement(query);
			pstmt.setInt(1, id);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				bean.setSignId(rs.getInt("row_id"));
				bean.setName(rs.getString("signatory_name"));
				bean.setPrivateKeyLabel(rs.getString("signature_private_key"));
				bean.setCertificateLabel(rs.getString("signature_certificate_key"));
				bean.setStartDate(rs.getDate("start_date"));
				bean.setEndDate(rs.getDate("end_date"));
				
			}
		} catch (SQLException exe) {
			logger.error(exe.toString());
			exe.printStackTrace(); 
		}catch (LunaException exe) {
			logger.error(exe.toString());
			System.err.println("The HSM Device is Down");
			exe.printStackTrace();
		} catch (Exception exe) {
			logger.error(exe.toString());
			exe.printStackTrace();
		} finally {
			DBConnectionUtil.releaseResources(pstmt, rs);
			DBConnectionUtil.releaseResources(dbConn);
		}
		System.out.println("Checking if private key is getting fetched");
		System.out.println(bean);
		return bean;
	}
	

	public void insertSignHistory(Integer profileId, ArrayList<String> documents, String  SignerName, String status) {
		//System.out.println("Writing in the db");
		Connection dbConn = null;
		String query = "INSERT INTO sign_history(profile_id, sign_document_name, signer_name,sign_status ) "
				+ "VALUES(?, ?, ?,?);";
		PreparedStatement pstmt = null;

		try {
			dbConn = DBConnectionUtil.getDBConnection();
			if (dbConn != null) {
				pstmt = dbConn.prepareStatement(query);
				
				for(String document:documents) {
					pstmt.setInt(1, profileId);
					pstmt.setString(2, document);
					pstmt.setString(3, SignerName);
					pstmt.setString(4, status);
					pstmt.addBatch();
				}
				
				pstmt.executeBatch();
				
				dbConn.commit();

			}
		} catch (Exception exe) {
			logger.error(exe.toString());
			exe.printStackTrace();
		} finally {
			DBConnectionUtil.releaseResources(pstmt, null);
			DBConnectionUtil.releaseResources(dbConn);
		}
		
	}
	
	public void insertSignHistory(Integer profileId, String document, String SignerName, String status, String reason) {
		//System.out.println("=====================Writing in the db=======================");
		Connection dbConn = null;
		String query = "INSERT INTO sign_history(profile_id, sign_document_name, signer_name,sign_status, failure_reason,sign_date) "
				+ "VALUES(?, ?, ?,?,?, CURRENT_TIMESTAMP);";
		PreparedStatement pstmt = null;

		try {
			dbConn = DBConnectionUtil.getDBConnection();
			if (dbConn != null) {

				pstmt = dbConn.prepareStatement(query);
				pstmt.setInt(1, profileId);
				pstmt.setString(2, document);
				pstmt.setString(3, SignerName);
				pstmt.setString(4, status);
				pstmt.setString(5, reason);
				pstmt.execute();
				
				dbConn.commit();

			}
		} catch (Exception exe) {
			logger.error(exe.toString());
			exe.printStackTrace();
		} finally {
			DBConnectionUtil.releaseResources(pstmt, null);
			DBConnectionUtil.releaseResources(dbConn);
		}
		
	}
	public List<ProfileMailBean> getProfileMail(Integer id) {
		ProfileMailBean bean = new ProfileMailBean();
		List<ProfileMailBean> list = new ArrayList<ProfileMailBean>();
		
		Connection dbConn = null;

		ResultSet rs = null;
		PreparedStatement pstmt = null;
		String query = "Select * from profile_mail where profile_id = ? and isActive = 1";

		try {
			dbConn = DBConnectionUtil.getDBConnection();
			pstmt = dbConn.prepareStatement(query);
			pstmt.setInt(1, id);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				bean = new ProfileMailBean();
				bean.setRowId(rs.getInt("row_id"));
				bean.setProfileId(rs.getInt("profile_id"));
				bean.setRecepientType(rs.getString("recepient_type"));
				bean.setEmailId(rs.getString("email_id"));
				
				list.add(bean);
			}
		} catch (Exception exe) {
			logger.error(exe.toString());
			exe.printStackTrace();
		} finally {
			DBConnectionUtil.releaseResources(pstmt, rs);
			DBConnectionUtil.releaseResources(dbConn);
		}

		return list;
	}
	
	public Integer getFailedCount(Integer id,Date date) {
		Integer failed = 0;
		Connection dbConn = null;

		ResultSet rs = null;
		PreparedStatement pstmt = null;
		String query = "Select count(*) from sign_history "+
				"where sign_status = 'FAILED' "+
				"and sign_date between '2018-12-03 00:00:00.0' and '2018-12-03 23:00:00.0'";

		try {
			dbConn = DBConnectionUtil.getDBConnection();
			pstmt = dbConn.prepareStatement(query);
			pstmt.setInt(1, id);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				failed = rs.getInt(1);
			}
		} catch (Exception exe) {
			logger.error(exe.toString());
			exe.printStackTrace();
		} finally {
			DBConnectionUtil.releaseResources(pstmt, rs);
			DBConnectionUtil.releaseResources(dbConn);
		}

		return failed;
	}
	
	public void insertMailHistory(Integer profileId, String document_name, String email_id, String status) {
		// System.out.println("Writing in the db");
		Connection dbConn = null;
		String query = "INSERT INTO tbl_email_history (profile_id, document_name, email_id, status, send_date) "
				+ "VALUES(?, ?, ?, ?, CURRENT_TIMESTAMP)";
		PreparedStatement pstmt = null;

		try {
			dbConn = DBConnectionUtil.getDBConnection();
			if (dbConn != null) {
				pstmt = dbConn.prepareStatement(query);

				pstmt.setInt(1, profileId);
				pstmt.setString(2, document_name);
				pstmt.setString(3, email_id);
				pstmt.setString(4, status);
				pstmt.addBatch();

				pstmt.execute();

				dbConn.commit();

			}
		} catch (Exception exe) {
			logger.error(exe.toString());
			exe.printStackTrace();
		} finally {
			DBConnectionUtil.releaseResources(pstmt, null);
			DBConnectionUtil.releaseResources(dbConn);
		}

	}

	public List<ContractTypeBean> getPendingContractTypeList() {
		List<ContractTypeBean> list = new ArrayList<>();
		ContractTypeBean bean = new ContractTypeBean();
		Connection dbConn = null;

		ResultSet rs = null;
		PreparedStatement pstmt = null;
		String query = "Select * from contract_type where contract_status = 'Y' and signed_contract_path = '#' ";
				
		try {
			dbConn = DBConnectionUtil.getDBConnection();
			pstmt = dbConn.prepareStatement(query);
			rs = pstmt.executeQuery();
			while(rs.next()) {
				bean = new ContractTypeBean();
				bean.setContractTypeId(rs.getInt("contract_type_id"));
				bean.setSourceFileName(rs.getString("uploaded_contract_document"));
				bean.setSourceFilePath(rs.getString("uploaded_contract_path"));
				bean.setProfileId(rs.getInt("profile_sig_id"));
				bean.setCreatedBy(rs.getInt("created_by"));
				bean.setUploadedBy(rs.getInt("uploaded_by"));
				
				list.add(bean);
			}
		} catch (Exception exe) {
			logger.error(exe.toString());
			exe.printStackTrace();
		} finally {
			DBConnectionUtil.releaseResources(pstmt, rs);
			DBConnectionUtil.releaseResources(dbConn);
		}

		return list;
	}
	
	public String getOutput(Integer id) {
		String outputFolder = "";
		Connection dbConn = null;

		ResultSet rs = null;
		PreparedStatement pstmt = null;
		String query = "Select * from profiles_main where profile_id = ? ";
				
		try {
			dbConn = DBConnectionUtil.getDBConnection();
			pstmt = dbConn.prepareStatement(query);
			pstmt.setInt(1, id);
			rs = pstmt.executeQuery();
			
			if(rs.next()) {
				
				outputFolder = rs.getString("output_folder");
				
			}
		} catch (Exception exe) {
			logger.error(exe.toString());
			exe.printStackTrace();
		} finally {
			DBConnectionUtil.releaseResources(pstmt, rs);
			DBConnectionUtil.releaseResources(dbConn);
		}

		return outputFolder;
	}
	
	public Integer getSignatoryId(Integer id) {
		Integer signId = 0;
		Connection dbConn = null;

		ResultSet rs = null;
		PreparedStatement pstmt = null;
		String query = "Select * from profile_signatories where profile_sig_id = ? ";
				
		try {
			dbConn = DBConnectionUtil.getDBConnection();
			pstmt = dbConn.prepareStatement(query);
			pstmt.setInt(1, id);
			rs = pstmt.executeQuery();
			
			if(rs.next()) {
				
				signId = rs.getInt("signatory_id_1");
			}
		} catch (Exception exe) {
			logger.error(exe.toString());
			exe.printStackTrace();
		} finally {
			DBConnectionUtil.releaseResources(pstmt, rs);
			DBConnectionUtil.releaseResources(dbConn);
		}

		return signId;
	}
	
	public SignatoriesBean getSignatureDetail(Integer signId) {
		SignatoriesBean signBean = new SignatoriesBean();
		
		Connection dbConn = null;

		ResultSet rs = null;
		PreparedStatement pstmt = null;
		String query = "Select * from signatories where signatories_id = ? and status = '1'";
				
		try {
			dbConn = DBConnectionUtil.getDBConnection();
			pstmt = dbConn.prepareStatement(query);
			pstmt.setInt(1, signId);
			rs = pstmt.executeQuery();
			
			if(rs.next()) {
				signBean.setName(rs.getString("signatories_name"));
				signBean.setCertificateLabel(rs.getString("certificate_key"));
				signBean.setPrivateKeyLabel(rs.getString("private_key"));
				
				signBean.setX(rs.getInt("x1"));
				signBean.setY(rs.getInt("y1"));
				signBean.setHeight(rs.getInt("height"));
				signBean.setWidth(rs.getInt("width"));
				signBean.setStartDate(rs.getDate("start_date"));
				signBean.setEndDate(rs.getDate("end_date"));
				
			}
		} catch (Exception exe) {
			logger.error(exe.toString());
			exe.printStackTrace();
		} finally {
			DBConnectionUtil.releaseResources(pstmt, rs);
			DBConnectionUtil.releaseResources(dbConn);
		}

		return signBean;
	}
	
	public Integer getEmployeeId(Integer userId) {
        Integer empId = null;
        Connection dbConn = null;

        ResultSet rs = null;
        PreparedStatement pstmt = null;
        String query = "Select employeeId from users where user_id = ? and status = '1'";

        try {
            dbConn = DBConnectionUtil.getDBConnection();
            pstmt = dbConn.prepareStatement(query);
            pstmt.setInt(1, userId);
            rs = pstmt.executeQuery();

            if(rs.next()) {
                empId = rs.getInt("employeeId");

            }
        } catch (Exception exe) {
            logger.error(exe.toString());
            exe.printStackTrace();
        } finally {
            DBConnectionUtil.releaseResources(pstmt, rs);
            DBConnectionUtil.releaseResources(dbConn);
        }

        return empId;
    } 
	


    public String getEmployeeMail(Integer empId) {
        String email = null;
        Connection dbConn = null;

        ResultSet rs = null;
        PreparedStatement pstmt = null;
        String query = "SELECT email FROM employee WHERE employee_id= ? and status = '1'";

        try {
            dbConn = DBConnectionUtil.getDBConnection();
            System.out.println("query -->"+query);
            System.out.println("dbConn ["+dbConn+"] empId "+empId);
            pstmt = dbConn.prepareStatement(query);
            pstmt.setInt(1, empId);
            rs = pstmt.executeQuery();

            if(rs.next()) {
                email = rs.getString("email");

            }
        } catch (Exception exe) {
            logger.error(exe.toString());
            exe.printStackTrace();
        } finally {
            DBConnectionUtil.releaseResources(pstmt, rs);
            DBConnectionUtil.releaseResources(dbConn);
        }

        return email;
    }
    public void updateContractType(Integer contractTypeId, String output) {
		// System.out.println("Writing in the db");
		Connection dbConn = null;
		String query = "UPDATE contract_type "
				+ "SET signed_contract_path=?, contract_status='S', STATUS='2', last_chg_by='SVC', last_chg_date=CURRENT_DATE, last_chg_time=CURRENT_TIME "
				+ "WHERE contract_type_id=? ";
		PreparedStatement pstmt = null;

		try {
			dbConn = DBConnectionUtil.getDBConnection();
			if (dbConn != null) {
				pstmt = dbConn.prepareStatement(query);
				pstmt.setString(1, output);
				pstmt.setInt(2, contractTypeId);
				pstmt.execute();
				dbConn.commit();
			}
		} catch (Exception exe) {
			logger.error(exe.toString());
			exe.printStackTrace();
		} finally {
			DBConnectionUtil.releaseResources(pstmt, null);
			DBConnectionUtil.releaseResources(dbConn);
		}

	}

}
